-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2020 at 07:10 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `name`, `email`, `body`, `created_at`) VALUES
(1, 1, 'diko the cat', 'diko_barj@hotmail.com', 'Hey! I like this post!!!', '2020-07-07 20:26:47'),
(2, 1, 'Mike The Mechanic', 'mikemechanic@gmail.com', 'Yes, I like it too!', '2020-07-07 20:32:50'),
(3, 1, 'Dennis Barnaja', 'dendakid@gmail.com', 'The  quick brown fox jumps over the lazy dog.', '2020-07-07 21:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `slug`, `body`, `deleted`, `created_at`) VALUES
(1, 1, 'Post One', 'post-one', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rutrum leo non tortor mollis, efficitur viverra tellus sollicitudin. Aliquam nec tristique ante. Vestibulum magna nunc, rutrum suscipit nibh ac, cursus congue nisi. Nam vitae bibendum mi. Phasellus nibh nisi, semper eget congue non, ultrices id nulla. Sed dictum erat nunc, eget tempus enim venenatis sodales. Etiam eget bibendum lectus. Donec rhoncus ante sit amet egestas faucibus. Suspendisse volutpat ullamcorper condimentum. Fusce facilisis massa id iaculis rutrum.', 0, '2020-07-07 17:54:59'),
(2, 1, 'Post Two', 'post-two', 'Praesent aliquam blandit ullamcorper. Fusce pulvinar urna eu porttitor dictum. Duis eget eleifend tellus. Mauris diam ipsum, lobortis ut venenatis non, lobortis et felis. Curabitur hendrerit tristique mollis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac libero id arcu tincidunt efficitur. Suspendisse potenti. Ut ut viverra nisl.', 0, '2020-07-07 18:00:23'),
(9, 1, 'Post Three', 'post-three', 'Maecenas elementum fermentum ex sit amet lobortis. Nam tincidunt lorem dolor. Vivamus at maximus diam. Aliquam eget arcu commodo, finibus lorem et, dictum augue. Donec gravida congue maximus. Cras tristique lectus sed risus porttitor, in rhoncus orci ullamcorper. Phasellus eros neque, convallis ornare felis nec, dictum ultricies augue. Nam est dolor, fringilla eu pellentesque vel, maximus eget ex. Aliquam erat volutpat. Sed mattis magna sem, eget scelerisque arcu tincidunt sit amet. In nisl sapien, egestas nec aliquet id, pretium volutpat metus. Morbi at risus eu lectus malesuada luctus. Integer tempus lacinia luctus. Sed et orci volutpat velit consectetur pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', 1, '2020-07-07 22:40:47'),
(10, 8, 'Post Threes', 'post-threes', 'This is first post as Alameth!', 0, '2020-07-08 11:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `registered_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `registered_date`) VALUES
(1, 'Dennis Barnaja', 'dennis_barnaja@yahoo.com', 'diko', '12345', '2020-07-07 15:48:36'),
(8, 'Alameth Barnaja', 'dennis.alameth@gmail.com', 'ala', '12345', '2020-07-08 11:03:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_title` (`title`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
