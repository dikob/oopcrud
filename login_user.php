<?php

session_start();
//including the database connection file
include_once("classes/Crud.php");
include_once("classes/Validation.php");

$crud = new Crud();
$validation = new Validation();
$_SESSION['POST'] = $_POST;

if (count($_POST) > 0) {
    // make sure inputs are sanitized
    $email = $crud->escape_string($_POST['email']);
    $password = $crud->escape_string($_POST['password']);

    // validate inputs
    $msg = $validation->check_empty($_POST, array('email', 'password'));
    $check_email = $validation->is_email_valid($_POST['email']);

    // checking empty fields
    if (!empty($msg)) {
        $_SESSION['error'] = $msg;
    } elseif (!$check_email) {
        $_SESSION['error'][] = 'Please provide proper email.';
    } else {
        // if all the fields are filled (not empty) 
        // insert data to database
        if (empty($_SESSION['error'])) {
            $result = $crud->getData("SELECT * FROM users WHERE email='$email' AND password='$password'");

            if (count($result) > 0) {
                $_SESSION['authenticated'] = true;
                $_SESSION['user'] = $result[0]['username'];
                $_SESSION['id'] = $result[0]['id'];
            }
        }
    }
    // redirect to index if ok otherwise back to form
    if (empty($_SESSION['error'])) {
        header("Location: index.php");
    } else {
        header("Location: login.php");
    }
    exit;
}
