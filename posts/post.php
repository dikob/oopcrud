<?php

include('../includes/header.php');

//including the database connection file
include(include_path("classes/Crud.php"));

$crud = new Crud();

$parse = parse_url($_SERVER['REQUEST_URI']);
$purl = explode('/', $parse['path']);

$slug = $purl[3];

//fetching the post in descending order (lastest entry first)
$query = "SELECT * FROM posts WHERE slug = '$slug' ORDER BY id DESC";
$result = $crud->getData($query);

$post = $result[0];
?>

<h2><?php echo $post['title']; ?></h2>
<small class="post-date">Posted on: <?php echo $post['created_at']; ?></small><br />
<div class="post-body">
    <?php echo $post['body']; ?>
</div>

<?php if (isset($_SESSION['id']) && $_SESSION['id'] == $post['user_id']) : ?>
    <hr>
    <a class="btn btn-primary float-left mr-3" href="<?php echo base_url() . 'posts/edit.php?pid=' . $post['id']; ?>">Edit</a>
    <form action="delete.php" method="post" id="delForm" name="delForm">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete</button>
    </form>
<?php endif; ?>


<?php

$pid = $post['id'];

//fetching the comments for this post
$query = "SELECT * FROM comments WHERE post_id = '$pid' ORDER BY id DESC";
$comments = $crud->getData($query);

?>
<hr>
<h3>Comments</h3>
<?php if ($comments) : ?>
    <?php foreach ($comments as $comment) : ?>
        <div class="card card-body bg-light mb-1">
            <h5><?php echo $comment['body']; ?> [by <strong><?php echo $comment['name']; ?></strong>]</h5>
        </div>
    <?php endforeach; ?>
<?php else : ?>
    <p>No comments To Display</p>
<?php endif; ?>

<hr>
<h3>Add Comment</h3>
<form action="comment.php" method="post" id="commentForm" name="commentForm" class="needs-validation" novalidate>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" value="<?php echo $_SESSION['POST']['name'] ?? ''; ?>" required />
        <div class="invalid-feedback">
            Please enter your name.
        </div>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" value="<?php echo $_SESSION['POST']['email'] ?? ''; ?>" required />
        <div class="invalid-feedback">
            Please enter your email address.
        </div>
    </div>
    <div class="form-group">
        <label for="body">Body</label>
        <textarea class="form-control" rows="6" id="editor1" name="body" required><?php echo $_SESSION['POST']['body'] ?? ''; ?></textarea>
        <div class="invalid-feedback">
            Please enter your comment.
        </div>
    </div>
    <input type="hidden" name="pid" value="<?php echo $post['id']; ?>" />
    <input type="hidden" name="slug" value="<?php echo $post['slug']; ?>" />
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Delete Post</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Are you sure you want to delete this post?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <form action="delete.php" method="post" id="delForm" name="delForm">
                    <input type="hidden" name="pid" value="<?php echo $post['id']; ?>" />
                    <button type="submit" class="btn btn-primary">Yes, delete</button>
                </form>
            </div>

        </div>
    </div>
</div>

<?php include('../includes/footer.php'); ?>