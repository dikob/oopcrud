<?php

session_start();

// make sure user is logged in or redirect to login page
if (!$_SESSION['authenticated'] ||  !$_SESSION['id']) {
    $url = str_replace( 'posts/' . basename($_SERVER['REQUEST_URI']), 'logout.php', $_SERVER['REQUEST_URI'] );
    header("Location: $url");
    exit;
}
//including the database connection file
include('../helpers/functions.php');
include_once(include_path("classes/Crud.php"));
include_once(include_path("classes/Validation.php"));

$crud = new Crud();
$validation = new Validation();
unset($_SESSION['POST']);
$_SESSION['POST'] = $_POST;

if (count($_POST) > 0) {
    // make sure inputs are sanitized
    $user_id = $_SESSION['id'];
    $title = $crud->escape_string($_POST['title']);
    $body = $crud->escape_string($_POST['body']);
    $slug = str_replace(' ', '-', strtolower($title));

    // validate inputs
    $msg = $validation->check_empty($_POST, array('title', 'body'));

    // checking empty fields
    if (!empty($msg)) {
        $_SESSION['error'] = $msg;
    } else {
        // if all the fields are filled (not empty) 
        // insert data to database
        if (empty($_SESSION['error'])) {
            $result = $crud->execute("INSERT INTO posts(`user_id`,`title`,`body`,`slug`) VALUES('$user_id','$title','$body','$slug')");
            $_SESSION['message'] = 'You have successfully added a post!';
        }
    }
    // redirect back to the post if ok otherwise back to form
    if (empty($_SESSION['error'])) {
        $url = str_replace(basename($_SERVER['REQUEST_URI']), '', $_SERVER['REQUEST_URI']);
    } else {
        $url = str_replace(basename($_SERVER['REQUEST_URI']), 'create.php', $_SERVER['REQUEST_URI']);
    }
    header("Location: $url");
    exit;
}
