<?php

session_start();
//including the database connection file
include('../helpers/functions.php');
include_once(include_path("classes/Crud.php"));
include_once(include_path("classes/Validation.php"));

$crud = new Crud();
$validation = new Validation();
unset($_SESSION['POST']);
$_SESSION['POST'] = $_POST;

if (count($_POST) > 0) {
    // make sure inputs are sanitized
    $name = $crud->escape_string($_POST['name']);
    $email = $crud->escape_string($_POST['email']);
    $body = $crud->escape_string($_POST['body']);
    $pid = $crud->escape_string($_POST['pid']);

    // validate inputs
    $msg = $validation->check_empty($_POST, array('name', 'email', 'body'));
    $check_email = $validation->is_email_valid($_POST['email']);

    // checking empty fields
    if (!empty($msg)) {
        $_SESSION['error'] = $msg;
    } elseif (!$check_email) {
        $_SESSION['error'][] = 'Please provide proper email.';
    } else {
        // if all the fields are filled (not empty) 
        // insert data to database
        if (empty($_SESSION['error'])) {
            $result = $crud->execute("INSERT INTO comments(post_id,name,email,body) VALUES('$pid','$name','$email','$body')");
            $_SESSION['message'] = 'You have successfully added a comment!';
        }
    }
    // redirect back to the post if ok otherwise back to form
    $url = str_replace(basename($_SERVER['REQUEST_URI']), $_POST['slug'], $_SERVER['REQUEST_URI']);
    header("Location: $url");
    exit;
}
