<?php

session_start();
//including the database connection file
include('../helpers/functions.php');
include_once(include_path("classes/Crud.php"));
include_once(include_path("classes/Validation.php"));

$crud = new Crud();
$validation = new Validation();
unset($_SESSION['POST']);
$_SESSION['POST'] = $_POST;

if (count($_POST) > 0) {
    // make sure inputs are sanitized
    $title = $crud->escape_string($_POST['title']);
    $body = $crud->escape_string($_POST['post_body']);
    $pid = $crud->escape_string($_POST['id']);
    $slug = str_replace(' ', '-', strtolower($title));

    // validate inputs
    $msg = $validation->check_empty($_POST, array('title', 'slug', 'post_body'));

    // checking empty fields
    if (!empty($msg)) {
        $_SESSION['error'] = $msg;
    } else {
        // if all the fields are filled (not empty) 
        // update data to database
        if (empty($_SESSION['error'])) {
            $result = $crud->execute("UPDATE posts set title='$title', slug='$slug', body='$body' WHERE id='$pid'");
        }
    }
    // redirect back to the post if ok otherwise back to form
    $url = str_replace(basename($_SERVER['REQUEST_URI']), $slug, $_SERVER['REQUEST_URI']);
    header("Location: $url");
    exit;
}
