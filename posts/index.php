<?php

include('../includes/header.php');

//including the database connection file
include_once(include_path("classes/Crud.php"));

$crud = new Crud();

//fetching data in descending order (lastest entry first)
$query = "SELECT * FROM posts WHERE deleted=0 ORDER BY id DESC";
$posts = $crud->getData($query);
?>

<?php foreach ($posts as $post) : ?>
    <h3><?php echo $post['title']; ?></h3>
    <div class="row">
        <div class="col-md-12">
            <small class="post-date">Posted on: <?php echo $post['created_at']; ?></small><br />
            <?php echo limit_text($post['body'], 70); ?>
            <p><a class="btn btn-secondary mt-2" href="<?php echo base_url('posts/' . $post['slug']) ?>">Read More...</a></p>
        </div>
    </div>
<?php endforeach; ?>


<?php include('../includes/footer.php'); ?>