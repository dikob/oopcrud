<?php

include('../includes/header.php');

//including the database connection file
include(include_path("classes/Crud.php"));

$crud = new Crud();

$user_id = $_SESSION['id'];

//fetching the post in descending order (lastest entry first)
$query = "SELECT * FROM posts WHERE user_id = '$user_id' AND deleted=0 ORDER BY id DESC";
$posts = $crud->getData($query);

?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">

                    <a href="<?php echo base_url() . 'posts/create.php' ?>" class="btn btn-primary mb-1">Create Post</a>

                    <?php if (count($posts) > 0) : ?>
                        <h3>Your Blog Posts</h3>
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($posts as $post) : ?>
                                    <tr>
                                        <td><?php echo $post['title'] ?></td>
                                        <td><a href="<?php echo base_url() . 'posts/edit.php?pid=' . $post['id']; ?>" class="btn btn-secondary">Edit</a></td>
                                        <td>

                                            <button type="button" value="<?php echo $post['id']; ?>" class="btn btn-danger" id="btn_del" data-toggle="modal" data-target="#myModal">Delete</button>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else : ?>
                        <p class="mt-3">You don't have any post!</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Delete Post</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Are you sure you want to delete this post?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <form action="delete.php" method="post" id="delForm" name="delForm">
                    <input type="hidden" name="pid" id="pid" value="<?php echo $post['id']; ?>" />
                    <button type="submit" class="btn btn-primary">Yes, delete</button>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    $("#btn_del").click(function() {
        $("#pid").val(this.value);
    });
</script>
<?php include('../includes/footer.php'); ?>