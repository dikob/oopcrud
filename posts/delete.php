<?php

session_start();
//including the database connection file
include('../helpers/functions.php');
include_once(include_path("classes/Crud.php"));
include_once(include_path("classes/Validation.php"));

$crud = new Crud();
$validation = new Validation();
unset($_SESSION['POST']);
$_SESSION['POST'] = $_POST;

if (count($_POST) > 0) {
    // make sure inputs are sanitized
    $pid = $crud->escape_string($_POST['pid']);
    // update data to database by soft delete
    $result = $crud->execute("UPDATE posts set deleted=1 WHERE id='$pid'");

    // redirect back to the post if ok otherwise back to blog
    $url = str_replace(basename($_SERVER['REQUEST_URI']), '', $_SERVER['REQUEST_URI']);
    header("Location: $url");
    exit;
}
