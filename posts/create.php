<?php include('../includes/header.php'); ?>

<h2>Write Your Story</h2>

<form action="create_post.php" method="post" id="createForm" name="createForm" class="needs-validation" novalidate>
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" name="title" placeholder="Enter Title" value="<?php echo $_SESSION['POST']['title'] ?? ''; ?>" required>
    <div class="invalid-feedback">
        Please enter the title for this story.
    </div>
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control" rows="6" id="editor1" name="body" placeholder="Enter Body" required><?php echo $_SESSION['POST']['body'] ?? ''; ?></textarea>
    <div class="invalid-feedback">
        Please enter your story.
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

<?php include('../includes/footer.php'); ?>