<?php

include('../includes/header.php');

//including the database connection file
include(include_path("classes/Crud.php"));

$crud = new Crud();

//fetching data in descending order (lastest entry first)
$pid = (int) $_GET['pid'];
$query = "SELECT * FROM posts WHERE id = '$pid' ORDER BY id DESC";
$result = $crud->getData($query);
$post = $result[0];
?>

<h2>Edit Your Story</h2>

<form action="edit_post.php" method="post" id="editForm" name="editForm" class="needs-validation" novalidate>
    <input type="hidden" name="id" value="<?php echo $post['id']; ?>" />
    <input type="hidden" name="slug" value="<?php echo $post['slug']; ?>" />
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Title" value="<?php echo $post['title']; ?>" required>
        <div class="invalid-feedback">
            Please enter the title for this story.
        </div>
    </div>
    <div class="form-group">
        <label for="body">Body</label>
        <textarea class="form-control" rows="6" id="editor1" name="post_body" placeholder="Enter Body" required><?php echo $post['body']; ?></textarea>
        <div class="invalid-feedback">
            Please enter your story.
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<?php include('../includes/footer.php'); ?>