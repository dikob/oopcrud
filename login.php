<?php

include('includes/header.php');

if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) {
    header("Location: index.php");
    exit;
}
?>

<form action="login_user.php" method="post" id="loginForm" name="loginForm" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-4 mx-auto">
            <h3 class="text-center">Login</h3>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Enter Email" required />
                <div class="invalid-feedback">
                    Please enter your email address.
                </div>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Enter Password" required />
                <div class="invalid-feedback">
                    Please enter your password.
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Login</button>
        </div>
    </div>
</form>

<?php include('includes/footer.php'); ?>