<?php

session_start();

$parse = parse_url(dirname($_SERVER['PHP_SELF']));
$purl = explode('/', $parse['path']);
array_shift($purl);

$path = '';
foreach ($purl as $el) {
  $path .= '.';
}

include($path . '/' . 'helpers/functions.php');
?>
<html>

<head>
  <title>Our Blog</title>
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap.bootswatch.flatly.css'; ?>" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/style.css'; ?>" type="text/css" media="screen" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="<?php echo base_url() ?>">Our Blog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <?php if (isset($_SESSION['authenticated'])) : ?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url() . 'posts/dashboard.php' ?>">Dashboard</a>
        </li>
        <?php endif; ?>
      </ul>
      <ul class="navbar-nav float-right">
        <?php if (!isset($_SESSION['authenticated'])) : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() . 'login.php' ?>">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() . 'register.php' ?>">Register</a>
          </li>
        <?php endif; ?>
        <?php if (isset($_SESSION['authenticated'])) : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() . 'posts/dashboard.php' ?>">hi <?php echo ucfirst($_SESSION['user']); ?>!</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() . 'posts/create.php' ?>">Create Post</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() . 'logout.php' ?>">Logout</a>
          </li>
        <?php endif; ?>
      </ul>
    </div>
  </nav>

  <div class="container pt-5">

    <?php if (isset($_SESSION['error']) && count($_SESSION['error']) > 0) : ?>
      <div class="alert alert-danger" role="alert">
        <?php foreach ($_SESSION['error'] as $error) : ?>
          <?php echo $error . '<br/>'; ?>
        <?php endforeach; ?>
      </div>
    <?php elseif (isset($_SESSION['message'])) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $_SESSION['message']; ?>
      </div>
    <?php endif; ?>