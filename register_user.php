<?php

session_start();
//including the database connection file
include_once("classes/Crud.php");
include_once("classes/Validation.php");

$crud = new Crud();
$validation = new Validation();
$_SESSION['POST'] = $_POST;

if (count($_POST) > 0) {
	// make sure inputs are sanitized
	$name = $crud->escape_string($_POST['name']);
	$email = $crud->escape_string($_POST['email']);
	$username = $crud->escape_string($_POST['username']);
	$password = $crud->escape_string($_POST['password']);
	$confirm_password = $crud->escape_string($_POST['confirm_password']);

	// validate inputs
	$msg = $validation->check_empty($_POST, array('name', 'email', 'username', 'password', 'confirm_password'));
	$check_email = $validation->is_email_valid($_POST['email']);

	// checking empty fields
	if (!empty($msg)) {
		$_SESSION['error'] = $msg;
	} elseif (!$check_email) {
		$_SESSION['error'][] = 'Please provide proper email.';
	} else {
		// if all the fields are filled (not empty) 
		// insert data to database
		if (empty($_SESSION['error'])) {
			$result = $crud->execute("INSERT INTO users(name,email,username,password) VALUES('$name','$email','$username','$password')");
		}
	}
	// redirect to index if ok otherwise back to form
	if (empty($_SESSION['error'])) {
		header("Location: index.php");
	} else {
		header("Location: register.php");
	}
	exit;
}
