
<?php include('includes/header.php'); ?>

<form action="register_user.php" method="post" id="regForm" name="regForm" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-4 mx-auto">
            <h3 class="text-center">Registration</h3>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Name" value="<?php echo $_SESSION['POST']['name'] ?? ''; ?>" required />
                <div class="invalid-feedback">
                    Please enter your name.
                </div>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $_SESSION['POST']['email'] ?? ''; ?>" required />
                <div class="invalid-feedback">
                    Please enter your email address.
                </div>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $_SESSION['POST']['username'] ?? ''; ?>" required />
                <div class="invalid-feedback">
                    Please enter your username.
                </div>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required />
                <div class="invalid-feedback">
                    Please enter your password.
                </div>
            </div>
            <div class="form-group">
                <label for="password2">Confirm Password</label>
                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required />
                <div class="invalid-feedback">
                    Please confirm your password.
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
    </div>
</form>

<?php include('includes/footer.php'); ?>