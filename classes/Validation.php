<?php
class Validation
{
	public function check_empty($data, $fields)
	{
		$msg = array();
		foreach ($fields as $value) {
			if (empty($data[$value])) {
				$msg[] = str_replace('_', ' ', $value) . " field empty";
			}
		}
		return $msg;
	}

	public function is_email_valid($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}
		return false;
	}
}
