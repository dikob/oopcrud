Simple Create, Read, Update, Delete (CRUD) app in PHP & MySQL using Object Oriented Programming (OOP)
========

A simple and basic blog system to add, edit, delete and view using PHP and MySQL using OOP.

SQL script to create database and tables is present in **database.sql** file.

