<?php

// base url
function base_url($subs = '')
{
    $url = 'http://localhost' . dirname($_SERVER['PHP_SELF']) . '/';
    $parse = parse_url($url);
    $purl = explode('/', $parse['path']);

    return 'http://localhost' . '/' . $purl[1] . '/' . $subs;
}

// limits the texts
function limit_text($text, $limit)
{
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

// returns the correct dotted path anywhere from the system
function include_path($filepath)
{
    $parse = parse_url(dirname($_SERVER['PHP_SELF']));
    $purl = explode('/', $parse['path']);
    array_shift($purl);

    $path = '';
    foreach ($purl as $el) {
        $path .= '.';
    }

    return $path . '/' . $filepath;
}
